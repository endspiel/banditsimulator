package config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import org.yaml.snakeyaml.Yaml;

public class ConfigUtil {
	public static String loadConfigAsString(String path) {
		StringBuilder result = new StringBuilder();
		File file = new File(path);
		try (FileReader fileReader = new FileReader(file)) {
			BufferedReader br = new BufferedReader(fileReader);
			String line;
			while ((line = br.readLine()) != null) {
				result.append(line).append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();
	}

	public static <T> T loadYamlConfig(String path, Class<T> c) {
		String configString = loadConfigAsString(path);
		Yaml yaml = new Yaml();
		T t = yaml.loadAs(configString, c);
		return t;
	}

}
