package config;

@lombok.Data
public class CreativeGeneratorConfig {
	private NDParams vctr;
	private NDParams cvr;

	@lombok.Data
	public static class NDParams {
		private double mu;
		private double error;
	}
}
