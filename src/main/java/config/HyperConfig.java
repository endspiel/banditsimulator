package config;

import java.util.List;

@lombok.Data
public class HyperConfig {
	private String configPath;
	private List<Integer> numCreativeList;
}
