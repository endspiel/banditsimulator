package config;

@lombok.Data
public class Config {
	private int numLoop;
	private int numCreative;
	private CreativeGeneratorConfig creativeGenerator;
	private int numVimp;
	private int loggingPeriodVimp;
	private String logDir;

	private CreativeSelectorConfig creativeSelector;

}
