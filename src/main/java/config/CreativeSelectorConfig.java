package config;

@lombok.Data
public class CreativeSelectorConfig {
	private String name;
	private int batchVimp;
	private double vctrPriorErrorInverseSquared;
	private double cvrPriorErrorInverseSquared;
	private boolean useK11iBeta;
}
