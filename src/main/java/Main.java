import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import config.Config;
import config.ConfigUtil;
import config.HyperConfig;
import it.unimi.dsi.fastutil.ints.IntList;
import logging.LoggingService;
import model.CampaignResult;
import model.CampaignSummary;
import model.CreativeGenerator;
import model.CreativeSelector;
import model.LightCampaignResult;

public class Main {

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public static void main(String[] args) {
		String hyperConfigPath;
		if (args.length >= 1 ) {
			hyperConfigPath = args[0];
		} else {
			hyperConfigPath = "config/hyper_config.yaml";
		}
		HyperConfig hyperConfig = ConfigUtil.loadYamlConfig(hyperConfigPath, HyperConfig.class);
		Config config = ConfigUtil.loadYamlConfig(hyperConfig.getConfigPath(), Config.class);
		for (Integer numCreative : hyperConfig.getNumCreativeList()) {
			config.setNumCreative(numCreative);
			executeLoops(config);
		}
	}

	public static void executeLoops(Config config) {
		final int NUM_LOGGING = config.getNumVimp() / config.getLoggingPeriodVimp();

		Date startDate = new Date();
		LoggingService loggingService = new LoggingService(config);

		List<List<LightCampaignResult>> loopResult = IntStream.rangeClosed(1, config.getNumLoop()).parallel().mapToObj(loopId -> executeSingleEpisode(loopId, config)).collect(Collectors.toList());

		List<List<LightCampaignResult>> transposedResult = new ArrayList<>(NUM_LOGGING);
		for (int p = 0; p < NUM_LOGGING; p++) {
			List<LightCampaignResult> sameVimpLightCampaignResultList = new ArrayList<>(config.getNumLoop());
			transposedResult.add(sameVimpLightCampaignResultList);
			for (int l = 0; l < config.getNumLoop(); l++) {
				sameVimpLightCampaignResultList.add(loopResult.get(l).get(p));
			}
		}

		// 積み上げによる集計
		loggingService.writeln("rolling summary\n");
		for (List<LightCampaignResult> lightCampaignResultList : transposedResult) {
			CampaignSummary campaignSummary = new CampaignSummary(lightCampaignResultList);
			loggingService.writeln(campaignSummary.toString());
			System.out.println(campaignSummary);
		}

		// NUM_LOGGINGの単位ごとの集計
		loggingService.writeln("\nsummary by NUM_LOGGING\n");
		for (int i = 0; i < transposedResult.size(); i++) {
			CampaignSummary campaignSummary;
			if (i == 0) {
				campaignSummary = new CampaignSummary(transposedResult.get(0));
			} else {
				List<LightCampaignResult> lightCampaignResultListBefore = transposedResult.get(i - 1);
				List<LightCampaignResult> lightCampaignResultListAfter = transposedResult.get(i);
				List<LightCampaignResult> lightCampaignResultListDiff = new ArrayList<>(config.getNumLoop());
				for (int j = 0; j < config.getNumLoop(); j++) {
					LightCampaignResult before = lightCampaignResultListBefore.get(j);
					LightCampaignResult after = lightCampaignResultListAfter.get(j);
					LightCampaignResult diff = new LightCampaignResult(after.getVimp() - before.getVimp(), after.getClick() - before.getClick(), after.getCv() - before.getCv());
					lightCampaignResultListDiff.add(diff);
				}
				campaignSummary = new CampaignSummary(lightCampaignResultListDiff);
			}
			loggingService.writeln(campaignSummary.toString());
			System.out.println(campaignSummary);
		}

		loggingService.close();
		Date endDate = new Date();
		System.out.println(String.format("All job were finished elapsed seconds=%d", (endDate.getTime() - startDate.getTime()) / 1000));

	}

	public static List<LightCampaignResult> executeSingleEpisode(int loopId, Config config) {
		List<LightCampaignResult> result = new ArrayList<>(config.getNumVimp() / config.getLoggingPeriodVimp());

		Date startDate = new Date();
		System.out.println(String.format("loopId=%d was started at %s", loopId, dateFormat.format(startDate)));

		CreativeGenerator creativeGenerator = new CreativeGenerator(config.getCreativeGenerator());
		CampaignResult campaignResult = creativeGenerator.generate(config.getNumCreative());
		CreativeSelector creativeSelector = CreativeSelector.buildCreativeSelector(config.getCreativeSelector());

		int numBatch = 0;
		while (numBatch < config.getNumVimp() / config.getCreativeSelector().getBatchVimp()) {
			IntList numCreativeSelection = creativeSelector.selectBatch(campaignResult);
			campaignResult.update(numCreativeSelection);
			numBatch++;
			if (numBatch * config.getCreativeSelector().getBatchVimp() % config.getLoggingPeriodVimp() == 0) {
				result.add(campaignResult.getLightCampaignResult());
//				System.out.println(campaignResult);
			}
		}

		System.out.println(campaignResult);
		Date endDate = new Date();
		System.out.println(String.format("loopId=%d was finished at %s, elapsed seconds=%d", loopId, dateFormat.format(endDate), (endDate.getTime() - startDate.getTime()) / 1000));
		return result;
	}
}
