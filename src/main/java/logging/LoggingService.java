package logging;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.yaml.snakeyaml.Yaml;

import config.Config;

public class LoggingService {

	private FileWriter fileWriter;

	private static FileWriter getFileWriter(String logDir) throws IOException {
		FileWriter fileWriter;
		Date d = new Date();
		SimpleDateFormat d1 = new SimpleDateFormat("yyyyMMdd-HHmmss");
		String s = d1.format(d);
		File file;
		if (logDir != null) {
			file = new File(logDir + s + ".log");
		} else {
			file = new File("log/" + s + ".log");
		}
		fileWriter = new FileWriter(file);
		return fileWriter;
	}


	public LoggingService(Config config) {
		try {
			this.fileWriter = getFileWriter(config.getLogDir());
		} catch (IOException e) {
			e.printStackTrace();
		}
		Yaml y = new Yaml();
		writeln(y.dump(config));
		writeln("-----");
	}

	public void writeln(String string) {
		try {
			this.fileWriter.write(string);
			this.fileWriter.write("\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			this.fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
