package model;

@lombok.Data
public class LightCampaignResult {
	private int vimp;
	private int click;
	private int cv;

	public LightCampaignResult(int vimp, int click, int cv) {
		this.vimp = vimp;
		this.click = click;
		this.cv = cv;
	}
}
