package model;

import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.math3.distribution.BetaDistribution;

import biz.k11i.rng.BetaRNG;
import config.CreativeSelectorConfig;

public class SimpleProductCreativeSelector extends CreativeSelector {
	public SimpleProductCreativeSelector(CreativeSelectorConfig config) {
		super(config);
	}

	@Override
	protected double[][] getCreativeIdBatchIdScores(CampaignResult campaignResult) {
		campaignResult.update();
		BetaParam vctrPriorParam = getPriorParam(campaignResult.getVimp(), campaignResult.getClick(), config.getVctrPriorErrorInverseSquared());
		BetaParam cvrPriorParam = getPriorParam(campaignResult.getClick(), campaignResult.getCv(), config.getCvrPriorErrorInverseSquared());

		double result[][] = new double[campaignResult.creativeSize()][config.getBatchVimp()];

		for (int i = 0; i < campaignResult.creativeSize(); i++) {
			CreativeResult creativeResult = campaignResult.getCreativeResults().get(i);
			double batchVctrScores[] = getBatchScores(creativeResult.getVimp(), creativeResult.getClick(), vctrPriorParam);
			double batchCvrScores[] = getBatchScores(creativeResult.getClick(), creativeResult.getCv(), cvrPriorParam);
			double batchScores[] = new double[config.getBatchVimp()];
			for (int j = 0; j < config.getBatchVimp(); j++) {
				batchScores[j] = batchVctrScores[j] * batchCvrScores[j];
			}
			result[i] = batchScores;
		}

		return result;
	}

	private double[] getBatchScores(int trial, int reward, BetaParam priorBetaParam) {
		double posteriorReward = reward + priorBetaParam.reward;
		double posteriorTrial = trial + priorBetaParam.trial;

		if (config.isUseK11iBeta()) {
			double result[] = new double[config.getBatchVimp()];
			for (int j=0;j<config.getBatchVimp();j++) {
				result[j] = BetaRNG.FAST_RNG.generate(ThreadLocalRandom.current(), posteriorReward, posteriorTrial - posteriorReward);
			}
			return result;
		} else {
			BetaDistribution betaDistribution = new BetaDistribution(posteriorReward, posteriorTrial - posteriorReward);
			return betaDistribution.sample(config.getBatchVimp());
		}
	}


	@lombok.Data
	public static class BetaParam {
		double trial;
		double reward;

		public BetaParam(double trial, double reward) {
			this.trial = trial;
			this.reward = reward;
		}
	}

	private BetaParam getPriorParam(int trial, int reward, double priorErrorInverseSquared) {
		// データが少ないとき2%を目安にする
		double mu = (reward + 0.2) / (trial + 10.0);
		double nu = Math.max(1.0, priorErrorInverseSquared * (1.0 - mu) / mu - 1);
		return new BetaParam(nu, mu * nu);
	}


}
