package model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.distribution.NormalDistribution;

import config.CreativeGeneratorConfig;

@lombok.Data
public class CreativeGenerator {
	private CreativeGeneratorConfig config;
	private NormalDistribution vctrDistribution;
	private NormalDistribution cvrDistribution;

	public CreativeGenerator(CreativeGeneratorConfig config) {
		this.config = config;
		this.vctrDistribution = getNormalDistribution(config.getVctr());
		this.cvrDistribution = getNormalDistribution(config.getCvr());
	}

	public CampaignResult generate(int numCreatives) {
		List<CreativeResult> creativeResults = new ArrayList<>(numCreatives);
		for (int i = 0; i < numCreatives; i++) {
			creativeResults.add(new CreativeResult(generateOne(i)));
		}
		return new CampaignResult(creativeResults);
	}

	private Creative generateOne(int creativeId) {
		double vctr = Math.max(vctrDistribution.sample(), 0.0);
		double cvr = Math.max(cvrDistribution.sample(), 0.0);
		return new Creative(creativeId, vctr, cvr);
	}

	private static NormalDistribution getNormalDistribution(CreativeGeneratorConfig.NDParams params) {
		double mu = params.getMu();
		double error = params.getError();
		double sd = error * mu;
		return new NormalDistribution(mu, sd);
	}
}
