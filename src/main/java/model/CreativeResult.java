package model;

import java.util.concurrent.ThreadLocalRandom;

@lombok.Data
public class CreativeResult {
	private Creative creative;
	private int vimp;
	private int click;
	private int cv;

	public CreativeResult(Creative creative) {
		this.creative = creative;
		this.vimp = 0;
		this.click = 0;
		this.cv = 0;
	}

	public void update(int numVimp) {
		if (numVimp == 0) {
			return;
		}

		vimp += numVimp;

		ThreadLocalRandom random = ThreadLocalRandom.current();
		long numClick = random.doubles(numVimp).filter(value -> value < creative.getVctr()).count();

		if (numClick == 0) {
			return;
		}

		click += numClick;

		long numCv = random.doubles(numClick).filter(value -> value < creative.getCvr()).count();

		cv += numCv;

	}

}
