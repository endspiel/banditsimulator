package model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@lombok.Data
public class CampaignSummary {
	private double vimpAvg;
	private double clickAvg;
	private double cvAvg;
	private double vimpError;
	private double clickError;
	private double cvError;

	public CampaignSummary(List<LightCampaignResult> lightCampaignResultList) {

		int size = lightCampaignResultList.size();

		double vimpSum = 0.0;
		double vimpSquaredSum = 0.0;
		double clickSum = 0.0;
		double clickSquaredSum = 0.0;
		double cvSum = 0.0;
		double cvSquaredSum = 0.0;

		for (LightCampaignResult lcr : lightCampaignResultList) {
			double vimp = lcr.getVimp();
			vimpSum += vimp;
			vimpSquaredSum += vimp * vimp;

			double click = lcr.getClick();
			clickSum += click;
			clickSquaredSum += click * click;

			double cv = lcr.getCv();
			cvSum += cv;
			cvSquaredSum += cv * cv;
		}

		this.vimpAvg = vimpSum / size;
		this.clickAvg = clickSum / size;
		this.cvAvg = cvSum / size;

		this.vimpError = Math.sqrt((vimpSquaredSum / size - this.vimpAvg * this.vimpAvg) / size);
		this.clickError = Math.sqrt((clickSquaredSum / size - this.clickAvg * this.clickAvg) / size);
		this.cvError = Math.sqrt((cvSquaredSum / size - this.cvAvg * this.cvAvg) / size);
	}

	public String toString() {
		double data[] = new double[]{vimpAvg, clickAvg, cvAvg, vimpError, clickError, cvError};
		return Arrays.stream(data).mapToObj(d -> Double.toString(d)).collect(Collectors.joining(", "));
	}

}
