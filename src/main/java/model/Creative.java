package model;

@lombok.Data
public class Creative {
	private final int id;
	private final double vctr;
	private final double cvr;

	public Creative(int id, double vctr, double cvr) {
		this.id = id;
		this.vctr = vctr;
		this.cvr = cvr;
	}
}
