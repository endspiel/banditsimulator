package model;

import java.util.List;

import config.CreativeSelectorConfig;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;

public abstract class CreativeSelector {

	protected CreativeSelectorConfig config;

	public CreativeSelector(CreativeSelectorConfig config) {
		this.config = config;
	}

	public static CreativeSelector buildCreativeSelector(CreativeSelectorConfig config) {
		String name = config.getName();

		if (name.equals("simpleProduct")) {
			return new SimpleProductCreativeSelector(config);
		} else {
			return null;
		}
	}

	public IntList selectBatch(CampaignResult campaignResult) {
		double creativeIdBatchIdScores[][] = new double[campaignResult.creativeSize()][config.getBatchVimp()];
		creativeIdBatchIdScores = getCreativeIdBatchIdScores(campaignResult);
		double batchIdCreativeIdScores[][] = transpose(creativeIdBatchIdScores, campaignResult.creativeSize(), config.getBatchVimp());
		int result[] = new int[campaignResult.creativeSize()];
		for (int j = 0; j < config.getBatchVimp(); j++) {
			int selectedCreativeId = getMaxId(batchIdCreativeIdScores[j]);
			result[selectedCreativeId]++;
		}
		return new IntArrayList(result);
	}

	protected abstract double[][] getCreativeIdBatchIdScores(CampaignResult campaignResult);

	// m x n matrix => n x m matrix
	private static double[][] transpose(double[][] matrix, int m, int n) {
		double result[][] = new double[n][m];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				result[j][i] = matrix[i][j];
			}
		}
		return result;
	}

	private int getMaxId(double[] scores) {
		double maxScore = -Double.MAX_VALUE;
		int maxId = -1;
		for (int i = 0; i < scores.length; i++) {
			double score = scores[i];
			if (maxScore < score) {
				maxScore = score;
				maxId = i;
			}
		}
		return maxId;
	}


}
