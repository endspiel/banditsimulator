package model;

import java.util.List;

import it.unimi.dsi.fastutil.ints.IntList;

@lombok.Data
public class CampaignResult {
	private int vimp;
	private int click;
	private int cv;
	private List<CreativeResult> creativeResults;

	public CampaignResult(List<CreativeResult> creativeResults) {
		this.creativeResults = creativeResults;
		this.vimp = 0;
		this.click = 0;
		this.cv = 0;
	}

	public int creativeSize() {
		return creativeResults.size();
	}

	public void update() {
		int vimp = 0;
		int click = 0;
		int cv = 0;
		for (CreativeResult creativeResult : creativeResults) {
			vimp += creativeResult.getVimp();
			click += creativeResult.getClick();
			cv += creativeResult.getCv();
		}
		this.vimp = vimp;
		this.click = click;
		this.cv = cv;
	}

	public void update(IntList numCreativeSelection) {
		for (int i = 0; i < creativeSize(); i++) {
			int num = numCreativeSelection.getInt(i);
			creativeResults.get(i).update(num);
		}
		update();
	}

	public LightCampaignResult getLightCampaignResult() {
		return new LightCampaignResult(vimp, click, cv);
	}
}
